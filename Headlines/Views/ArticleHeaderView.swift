//
//  ArticleHeaderView.swift
//  Headlines
//
//  Created by Borja Santos-Díez on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import PureLayout
import SDWebImage
import UIKit

class ArticleHeaderView: UIView {

    private let imageView = UIImageView.newAutoLayout()
    private let headlineLabel = UILabel.newAutoLayout()

    init() {

        super.init(frame: .zero)
        self.configureView()
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    func configure(with imageURL: URL?, headline: String) {

        self.imageView.sd_setImage(with: imageURL)
        self.headlineLabel.text = headline
    }
}

// MARK: - Private methods
private extension ArticleHeaderView {

    enum Constants {

        static let headlineMargin: CGFloat = 20
    }

    func configureView() {

        self.addSubviews()
        self.defineViewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.addSubviews(self.imageView,
                         self.headlineLabel)
    }

    func defineViewConstraints() {

        self.imageView.autoPinEdgesToSuperviewEdges()

        [ALEdge.left, ALEdge.right, ALEdge.bottom].forEach {

            self.headlineLabel.autoPinEdge(toSuperviewEdge: $0, withInset: Constants.headlineMargin)
        }
    }

    func configureSubviews() {

        self.backgroundColor = .black

        self.imageView.alpha = 0.6
        self.imageView.contentMode = .scaleAspectFill

        self.headlineLabel.font = UIFont.headlineFont
        self.headlineLabel.textColor = .white
        self.headlineLabel.numberOfLines = 0
        self.headlineLabel.lineBreakMode = .byWordWrapping
    }
}
