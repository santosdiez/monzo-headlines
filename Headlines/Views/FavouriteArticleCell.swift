//
//  FavouriteArticleCell.swift
//  Headlines
//
//  Created by Borja on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation
import UIKit

class FavouriteArticleCell: UITableViewCell {

    private static let dateFormatter: DateFormatter = {

        let formatter = DateFormatter()
        // Set locale-specific format
        formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: Constants.dateFormat,
                                                        options: 0,
                                                        locale: Locale.current)

        return formatter
    }()

    private let thumbnail = UIImageView.newAutoLayout()
    private let titleLabel = UILabel.newAutoLayout()
    private let subtitleLabel = UILabel.newAutoLayout()
    private let separator = UIView.newAutoLayout()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {

        super.init(style: .default, reuseIdentifier: reuseIdentifier)

        self.selectionStyle = .none

        self.configureView()
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    func configure(with article: Article) {

        self.thumbnail.sd_setImage(with: article.imageURL)
        self.titleLabel.text = article.headline
        self.subtitleLabel.text = FavouriteArticleCell.dateFormatter.string(from: article.published)
    }
}

// MARK: - Private methods
private extension FavouriteArticleCell {

    enum Constants {

        static let dateFormat = "dMMYYYY"
        static let horizontalMargin: CGFloat = 20
        static let verticalMargin: CGFloat = 18
        static let thumbnailSize: CGFloat = 80
    }

    func configureView() {

        self.addSubviews()
        self.defineViewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.contentView.addSubviews(self.thumbnail,
                                     self.titleLabel,
                                     self.subtitleLabel,
                                     self.separator)
    }

    func defineViewConstraints() {

        self.thumbnail.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .trailing)
        self.thumbnail.autoMatch(.width, to: .height, of: self.contentView)
        self.thumbnail.autoSetDimension(.height, toSize: Constants.thumbnailSize)

        self.titleLabel.autoPinEdge(.leading, to: .trailing, of: self.thumbnail, withOffset: Constants.horizontalMargin)
        self.titleLabel.autoPinEdgesToSuperviewEdges(named: .top, .trailing,
                                                     withInsets: UIEdgeInsets(top: Constants.verticalMargin,
                                                                              left: 0,
                                                                              bottom: 0,
                                                                              right: Constants.horizontalMargin))

        self.subtitleLabel.autoPinEdge(.leading, to: .leading, of: self.titleLabel)
        self.subtitleLabel.autoPinEdge(.trailing, to: .trailing, of: self.titleLabel)
        self.subtitleLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: Constants.verticalMargin)

        self.separator.autoPinEdgesToSuperviewEdges(named: .leading, .bottom, .trailing)
        self.separator.autoSetDimension(.height, toSize: 1)
    }

    func configureSubviews() {

        self.thumbnail.contentMode = .scaleAspectFill
        self.thumbnail.clipsToBounds = true
        self.titleLabel.font = UIFont.cellFontTitle
        self.subtitleLabel.font = UIFont.cellFontSubtitle
        self.separator.backgroundColor = .groupTableViewBackground
    }
}
