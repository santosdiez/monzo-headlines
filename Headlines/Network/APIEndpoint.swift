//
//  APIEndpoint.swift
//  Headlines
//
//  Created by Borja Santos-Díez on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation

enum HTTPMethod: String {

    case GET
    case POST
    case PUT
    case PATCH
    case DELETE
    case HEAD
}

enum APIEndpoint {

    static let apiKey = "enj8pstqu5yat6yesfsdmd39"
    static let scheme = "http"
    static let host = "content.guardianapis.com"

    case articles

    private var path: String {

        switch self {

        case .articles:
            return "search"
        }
    }

    private var queryParams: [String: String] {

        switch self {

        case .articles:
            return [
                "q": "fintech",
                "show-fields": "main,body"
            ]
        }
    }

    var httpMethod: HTTPMethod {

        switch self {

        case .articles: return .GET
        }
    }

    var url: URL? {

        var urlComponents = URLComponents()
        urlComponents.scheme = APIEndpoint.scheme
        urlComponents.host = APIEndpoint.host
        urlComponents.path = "/\(self.path)"

        var queryParams = ["api-key": APIEndpoint.apiKey]

        queryParams.merge(self.queryParams) { (_, new) in new }

        let params = queryParams.map { key, value in

            return URLQueryItem(name: key, value: value)
        }

        urlComponents.queryItems = params

        return urlComponents.url
    }

}
