//
//  ViewController.swift
//  Headlines
//
//  Created by Joshua Garnham on 09/05/2017.
//  Copyright © 2017 Example. All rights reserved.
//

import PureLayout
import UIKit
import SVProgressHUD

class MainViewController: UIPageViewController {

    private var controllers: [ArticleViewController] = []

    init() {

        super.init(transitionStyle: .pageCurl,
                   navigationOrientation: .horizontal,
                   options: nil)

        self.dataSource = self
    }

    required init?(coder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.loadData()
    }
}

// MARK: - Private methods
private extension MainViewController {

    func loadData() {

        SVProgressHUD.show()

        DataProvider.articles.loadArticles().done { articles in

            self.controllers = articles.map { ArticleViewController(with: $0) }

            SVProgressHUD.popActivity()

            guard let viewController = self.controllers.first else { return }

            self.setViewControllers([viewController],
                                    direction: .forward,
                                    animated: true,
                                    completion: nil)
            }
            .catch { error in

                SVProgressHUD.showError(withStatus: nil)
        }
    }
}

// MARK: - UIPageViewControllerDataSource
extension MainViewController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let controller = viewController as? ArticleViewController,
            let index = self.controllers.firstIndex(of: controller),
            index > 0,
            self.controllers.count > 1 else { return nil }

        return self.controllers[index - 1]
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {

        guard let controller = viewController as? ArticleViewController,
            let index = self.controllers.firstIndex(of: controller),
            index < self.controllers.count - 1,
            self.controllers.count > 1 else { return nil }

        return self.controllers[index + 1]
    }
}
