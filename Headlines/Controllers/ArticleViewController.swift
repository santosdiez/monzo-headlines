//
//  ArticleViewController.swift
//  Headlines
//
//  Created by Borja Santos-Díez on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation
import UIKit

class ArticleViewController: UIViewController {

    private var article: Article

    private let headerView = ArticleHeaderView.newAutoLayout()
    private let bodyText: UITextView = {

        let textView = UITextView.newAutoLayout()
        textView.isEditable = false
        textView.isScrollEnabled = false

        return textView
    }()

    private let starButton = UIButton(type: .custom).configureForAutoLayout()
    private let favouritesButton = UIButton(type: .system).configureForAutoLayout()

    private let scrollView = UIScrollView.newAutoLayout()

    private let stackView: UIStackView = {

        let stackView = UIStackView.newAutoLayout()
        stackView.axis = .vertical

        return stackView
    }()

    init(with article: Article) {

        self.article = article
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        self.configureView()
        self.configure(with: self.article)
    }
}

// MARK: - Private methods
private extension ArticleViewController {

    enum Constants {

        static let headerHeight: CGFloat = 400
        static let bodyMargin: CGFloat = 20
    }

    func configureView() {

        self.addSubviews()
        self.defineViewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        let buttonsView = UIView.newAutoLayout()
        buttonsView.addSubviews(self.starButton,
                                self.favouritesButton)

        self.stackView.addArrangedSubviews(self.headerView,
                                           self.bodyText,
                                           buttonsView)

        self.scrollView.addSubview(self.stackView)
        self.view.addSubview(self.scrollView)
    }

    func defineViewConstraints() {

        self.scrollView.autoPinEdgesToSuperviewEdges()

        self.stackView.autoPinEdgesToSuperviewEdges(with: .zero)
        self.stackView.autoMatch(.width, to: .width, of: self.scrollView)

        self.headerView.autoSetDimension(.height, toSize: Constants.headerHeight)

        self.starButton.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: Constants.bodyMargin,
                                                                        left: Constants.bodyMargin,
                                                                        bottom: Constants.bodyMargin,
                                                                        right: 0),
                                                     excludingEdge: .trailing)

        self.favouritesButton.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: Constants.bodyMargin,
                                                                              left: 0,
                                                                              bottom: Constants.bodyMargin,
                                                                              right: Constants.bodyMargin),
                                                           excludingEdge: .leading)
    }

    func configureSubviews() {

        self.view.backgroundColor = .white

        self.bodyText.backgroundColor = .white
        self.bodyText.font = UIFont.articleFont
        self.bodyText.textContainerInset = UIEdgeInsets(top: Constants.bodyMargin,
                                                        left: Constants.bodyMargin,
                                                        bottom: Constants.bodyMargin,
                                                        right: Constants.bodyMargin)

        self.starButton.setImage(self.article.isFavourite ? Asset.favouriteOn.image : Asset.favouriteOff.image,
                                 for: .normal)
        self.starButton.addTarget(self, action: #selector(starButtonPressed), for: .touchDown)

        self.favouritesButton.setTitle(L10n.buttonFavourites, for: .normal)
        self.favouritesButton.setTitleColor(.headlinesYellow, for: .normal)
        self.favouritesButton.addTarget(self, action: #selector(favouritesButtonPressed), for: .touchUpInside)
    }

    func configure(with article: Article) {

        self.headerView.configure(with: article.imageURL, headline: article.headline)
        self.bodyText.text = article.body
    }
}

// MARK: - UI interactions
private extension ArticleViewController {

    @objc
    func favouritesButtonPressed() {

        let vc = FavouritesViewController()
        let nc = UINavigationController(rootViewController: vc)

        present(nc, animated: true, completion: nil)
    }

    @objc
    func starButtonPressed() {

        self.article.toggleFavourite()
        let image = self.article.isFavourite ? Asset.favouriteOn.image : Asset.favouriteOff.image

        DataProvider.articles.saveArticle(self.article)

        // Animate icon change
        UIButton.animate(withDuration: 0.2, animations: {

            self.starButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)

        }, completion: { _ in

            UIButton.animate(withDuration: 0.2) {

                self.starButton.setImage(image, for: .normal)
                self.starButton.transform = CGAffineTransform.identity
            }
        })
    }
}
