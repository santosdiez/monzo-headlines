//
//  FavouritesViewController.swift
//  Headlines
//
//  Created by Joshua Garnham on 09/05/2017.
//  Copyright © 2017 Example. All rights reserved.
//

import PureLayout
import SDWebImage
import SVProgressHUD
import UIKit

class FavouritesViewController: UIViewController {

    private var articles: [Article] = []
    private var filteredArticles: [Article] = []

    private let tableView = UITableView.newAutoLayout()
    private let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {

        super.viewDidLoad()
        self.searchController.loadViewIfNeeded()
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false

        if #available(iOS 11.0, *) {

            navigationItem.searchController = self.searchController

        } else {

            self.searchController.hidesNavigationBarDuringPresentation = false
            self.searchController.searchBar.searchBarStyle = .minimal

            // Include the search bar within the navigation bar.
            self.navigationItem.titleView = self.searchController.searchBar
        }

        self.definesPresentationContext = true

        self.configureView()
        self.loadData()
    }

    func loadData() {

        SVProgressHUD.show()

        DataProvider.articles.loadFavourites().done { articles in

            self.articles = articles
            self.title = L10n.titleFavourites(String(describing: self.articles.count))
            self.tableView.reloadData()

            SVProgressHUD.popActivity()

        }.catch { error in

            SVProgressHUD.showError(withStatus: nil)
        }
    }
}

// MARK: - Private methods
private extension FavouritesViewController {

    enum Constants {

        static let cellIdentifier = "cell"
        static let rowHeight: CGFloat = 80
    }

    func configureView() {

        self.addSubviews()
        self.defineViewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.view.addSubview(self.tableView)
    }

    func defineViewConstraints() {

        self.tableView.autoPinEdgesToSuperviewEdges()
    }

    func configureSubviews() {

        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = Constants.rowHeight

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                                 target: self,
                                                                 action: #selector(doneButtonPressed))

        self.navigationItem.rightBarButtonItem?.tintColor = .headlinesYellow

        self.tableView.register(FavouriteArticleCell.self, forCellReuseIdentifier: Constants.cellIdentifier)
    }

    @objc
    func doneButtonPressed() {

        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UISearchResultsUpdating
extension FavouritesViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {

        guard let searchText = searchController.searchBar.text?.lowercased() else { return }

        self.filteredArticles = self.articles.filter { article in

            return article.headline.lowercased().contains(searchText) ||
                article.body.lowercased().contains(searchText)
        }

        self.tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension FavouritesViewController: UITableViewDataSource {

    func isFiltering() -> Bool {

        return self.searchController.isActive && (searchController.searchBar.text?.isEmpty ?? true) == false
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.isFiltering() ? self.filteredArticles.count : self.articles.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier,
                                                       for: indexPath) as? FavouriteArticleCell else {

                                                        // Should never happen
                                                        return UITableViewCell()
        }

        let articles = self.isFiltering() ? self.filteredArticles : self.articles
        let article = articles[indexPath.row]

        cell.configure(with: article)

        return cell
    }
}
