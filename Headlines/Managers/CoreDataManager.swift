//
//  PersistenceManager.swift
//  Headlines
//
//  Created by Borja on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import CoreData
import Foundation

struct CoreDataManager {

    static let container: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "Headlines")

        container.loadPersistentStores(completionHandler: { (_, error) in

            if let error = error {
                fatalError("Unable to Load Persistent Store")
            }
        })

        return container
    }()

    static var context: NSManagedObjectContext {

        return self.container.viewContext
    }

    static func fetchAll<T: NSManagedObject>(of type: T.Type) -> [T]? {

        let context = self.container.viewContext
        guard let request = type.fetchRequest() as? NSFetchRequest<T> else { return nil }

        return try? context.fetch(request)
    }

    static func deleteAll<T: NSManagedObject>(of type: T.Type) {

        let context = self.container.viewContext
        let request = type.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)

        _ = try? context.execute(deleteRequest)
        self.saveContext()
    }

    static func saveContext() {

        if self.container.viewContext.hasChanges {

            do {

                try self.container.viewContext.save()

            } catch {

                fatalError("An error occurred while saving: \(error)")
            }
        }
    }
}
