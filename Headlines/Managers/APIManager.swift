//
//  APIManager.swift
//  Headlines
//
//  Created by Borja Santos-Díez on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation
import PromiseKit

private struct ResponseWrapper<T: Codable>: Codable {

    var results: [T]

    private enum CodingKeys: String, CodingKey {

        case response
        case results
    }

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        let response = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .response)

        self.results = try response.decode([T].self, forKey: .results)
    }

    func encode(to encoder: Encoder) throws {

        // Implement if needed. At the moment, this struct
        // will only be used for decoding
    }
}

struct APIManager {

    private static let decoder: JSONDecoder = {

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601

        return decoder
    }()

    static func promiseForRequest<T: Codable>(to endpoint: APIEndpoint, with type: [T].Type) -> Promise<[T]> {

        guard let request = self.createRequest(with: endpoint) else {

            fatalError("Error creating request")
        }

        return Promise { seal in

            URLSession.shared.dataTask(.promise, with: request).validate()
                .compactMap(on: .global(qos: .background)) {

                    let data = try self.decoder.decode(ResponseWrapper<T>.self, from: $0.data)

                    seal.fulfill(data.results)

                }
                .catch { error in

                    fatalError(error.localizedDescription)
            }
        }
    }
}

private extension APIManager {

    static func createRequest(with endpoint: APIEndpoint) -> URLRequest? {

        guard let url = endpoint.url else { return nil }

        var request = URLRequest(url: url)
        request.httpMethod = endpoint.httpMethod.rawValue

        return request
    }
}
