//
//  Article.swift
//  Headlines
//
//  Created by Borja on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation

struct Article {

    let id: String
    let headline: String
    let imageURL: URL?
    let published: Date
    let body: String
    private(set) var isFavourite: Bool

    mutating func toggleFavourite() {

        self.isFavourite.toggle()
    }
}
