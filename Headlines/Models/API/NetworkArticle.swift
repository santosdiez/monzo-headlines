//
//  NetworkArticle.swift
//  Headlines
//
//  Created by Borja Santos-Díez on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation

private extension String {

    var strippingTags: String {

        var result = self.replacingOccurrences(of: "</p> <p>", with: "\n\n") as NSString

        var range = result.range(of: "<[^>]+>", options: .regularExpression)
        while range.location != NSNotFound {
            result = result.replacingCharacters(in: range, with: "") as NSString
            range = result.range(of: "<[^>]+>", options: .regularExpression)
        }

        return result as String
    }

    var url: URL? {

        guard let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else {

            return nil
        }

        let matches = detector.matches(in: self,
                                       options: [],
                                       range: NSRange(location: 0, length: (self as NSString).length))

        return matches.first?.url
    }
}

struct NetworkArticle: Codable {

    var id: String
    var published: Date
    var headline: String
    private var rawImageURL: String?
    var body: String

    var imageURL: URL? {

        guard let urlString = self.rawImageURL,
            let url = URL(string: urlString) else { return nil }

        return url
    }

    private enum CodingKeys: String, CodingKey {

        case id
        case published = "webPublicationDate"
        case headline = "webTitle"
        case fields
        // Fields
        case main
        case body
    }

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try container.decode(String.self, forKey: .id)
        self.published = try container.decode(Date.self, forKey: .published)
        self.headline = try container.decode(String.self, forKey: .headline)

        let fields = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .fields)

        // Process main field to get the image url
        self.rawImageURL = try fields.decode(String.self, forKey: .main).url?.absoluteString

        self.body = try fields.decode(String.self, forKey: .body).strippingTags
    }

    func encode(to encoder: Encoder) throws {

        // We don't currently need to encode
        // Articles won't be sent back to the server
    }
}
