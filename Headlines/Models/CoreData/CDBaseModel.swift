//
//  CDBaseModel.swift
//  Headlines
//
//  Created by Borja on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation

protocol CDBaseModel {

    associatedtype T
    associatedtype U

    func toModel() -> T?

    @discardableResult
    static func fromModel(_ model: U) -> Self
}
