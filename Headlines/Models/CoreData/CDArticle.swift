//
//  CDArticle.swift
//  Headlines
//
//  Created by Borja on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation
import CoreData

final class CDArticle: NSManagedObject, CDBaseModel {

    typealias T = Article
    typealias U = NetworkArticle

    func toModel() -> Article? {

        guard let id = self.id,
            let headline = self.headline,
            let published = self.published,
            let body = self.body else { return nil }

        var imageURL: URL?

        if let url = self.imageURL {

            imageURL = URL(string: url)
        }

        return Article(id: id,
                       headline: headline,
                       imageURL: imageURL,
                       published: published,
                       body: body,
                       isFavourite: self.isFavourite)
    }

    static func fromModel(_ model: NetworkArticle) -> CDArticle {

        // Check if the entity exists
        let fetchRequest: NSFetchRequest<CDArticle> = CDArticle.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", model.id)

        if let coreDataModels = try? CoreDataManager.context.fetch(fetchRequest),
            let article = coreDataModels.first {

            // It already exists. Do not create
            return article
        }

        let entity = CDArticle(context: CoreDataManager.container.viewContext)

        entity.id = model.id
        entity.headline = model.headline
        entity.imageURL = model.imageURL?.absoluteString
        entity.body = model.body
        entity.published = model.published
        entity.isFavourite = false

        return entity
    }
}
