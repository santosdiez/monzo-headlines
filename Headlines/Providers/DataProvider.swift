//
//  DataProvider.swift
//  Headlines
//
//  Created by Borja on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import Foundation
import PromiseKit

protocol ArticleDataProviderProtocol {

    func loadArticles() -> Promise<[Article]>
    func loadFavourites() -> Promise<[Article]>
    func saveArticle(_ article: Article)
}

struct DataProvider {

    static let articles: ArticleDataProviderProtocol = ArticleCoreDataProvider()
}
