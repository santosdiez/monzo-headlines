//
//  CoreDataProvider.swift
//  Headlines
//
//  Created by Borja on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import CoreData
import Foundation
import PromiseKit

struct CoreDataError: Error {}

struct ArticleCoreDataProvider: ArticleDataProviderProtocol {

    func loadArticles() -> Promise<[Article]> {

        // Fetch new articles
        return APIManager.promiseForRequest(to: .articles, with: [NetworkArticle].self)
            .then { articles -> Promise<[CDArticle]> in

                let coreDataArticles = articles.compactMap { CDArticle.fromModel($0) }
                CoreDataManager.saveContext()

                return Promise.value(coreDataArticles)
            }
            .then { _ -> Promise<[CDArticle]> in

                guard let models = CoreDataManager.fetchAll(of: CDArticle.self) else {

                    return Promise(error: CoreDataError())
                }

                // Sort by date descending
                return Promise.value(models.sorted(by: { art1, art2 in

                    guard let date1 = art1.published,
                        let date2 = art2.published else { return false }

                    return date1 > date2
                }))
            }
            .then { coreDataModels -> Promise<[Article]> in

                let models = coreDataModels.compactMap { $0.toModel() }

                return Promise.value(models)
        }
    }

    func loadFavourites() -> Promise<[Article]> {

        return Promise { seal in

            let fetchRequest: NSFetchRequest<CDArticle> = CDArticle.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "isFavourite == 1")

            guard let coreDataModels = try? CoreDataManager.context.fetch(fetchRequest) else {

                seal.reject(CoreDataError())
                return
            }

            let articles = coreDataModels.compactMap { $0.toModel() }

            return seal.fulfill(articles)
        }
    }

    func saveArticle(_ save: Article) {

        let fetchRequest: NSFetchRequest<CDArticle> = CDArticle.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", save.id)
        fetchRequest.fetchLimit = 1

        if let articles = try? CoreDataManager.context.fetch(fetchRequest),
            let article = articles.first {

            article.isFavourite = save.isFavourite
            CoreDataManager.saveContext()
        }
    }
}
