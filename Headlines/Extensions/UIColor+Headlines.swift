//
//  UIColor+Headlines.swift
//  Headlines
//
//  Created by Borja Santos-Díez on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import UIKit

extension UIColor {

    static let headlinesYellow = UIColor(red: 1, green: 205.0/255.0, blue: 0, alpha: 1)
}
