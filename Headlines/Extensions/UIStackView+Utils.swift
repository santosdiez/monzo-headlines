//
//  UIStackView+Utils.swift
//  Headlines
//
//  Created by Borja Santos-Díez on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import UIKit

extension UIStackView {

    public func addArrangedSubviews(_ subviews: UIView...) {

        subviews.forEach {

            self.addArrangedSubview($0)
        }
    }
}
