//
//  UIView+AutoLayout.swift
//  Headlines
//
//  Created by Borja Santos-Díez on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import PureLayout
import UIKit

extension UIView {

    func autoPinEdgesToSuperviewEdges(named: ALEdge..., withInsets insets: UIEdgeInsets? = nil) {

        let isRTL = UIView.appearance().semanticContentAttribute == .forceRightToLeft

        named.forEach { edge in

            let inset: CGFloat = {

                switch edge {

                case .leading: return (isRTL ? insets?.right : insets?.left) ?? 0
                case .trailing: return (isRTL ? insets?.left : insets?.right) ?? 0
                case .left: return insets?.left ?? 0
                case .right: return insets?.right ?? 0
                case .top: return insets?.top ?? 0
                case .bottom: return insets?.bottom ?? 0
                }
            }()

            self.autoPinEdge(toSuperviewEdge: edge, withInset: inset)
        }
    }
}
