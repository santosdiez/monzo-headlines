//
//  UIFont+Headlines.swift
//  Headlines
//
//  Created by Borja Santos-Díez on 21/05/2019.
//  Copyright © 2019 Example. All rights reserved.
//

import UIKit

extension UIFont {

    static let headlineFont = UIFont(name: "Superclarendon-Bold", size: 28)

    static let articleFont = UIFont(name: "Baskerville", size: 22)

    static let cellFontTitle = UIFont(name: "Superclarendon-Regular", size: 16)

    static let cellFontSubtitle = UIFont(name: "Baskerville", size: 16)
}
